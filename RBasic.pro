TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    RParser.cpp \
    RVar.cpp \
    RVec.cpp \
    RInt.cpp \
    RToken.cpp \
    RBasic.cpp

HEADERS += \
    RParser.h \
    RVar.h \
    RVec.h \
    RInt.h \
    RToken.h \
    RBasic.h

