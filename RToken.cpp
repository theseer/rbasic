#include "RToken.h"
#include "RInt.h"

RToken::RToken(const RToken& other) :
    type(other.type), data(other.data), priority(other.priority),
    minArgs(other.minArgs), maxArgs(other.maxArgs), argsExpected(other.argsExpected),
    isLeftAssociated(other.isLeftAssociated), basetype(other.basetype),
    vector(other.vector){}

RToken::RToken(const std::string& str, const bool plainString)
{
    data = str;

    if(plainString)
    {
        type = RT_RCHAR;
        basetype = RTB_CONST;
        return;
    }

    if(str == "TRUE")
    {
        type = RT_TRUE;
        basetype = RTB_CONST;
        return;
    }
    if(str == "FALSE")
    {
        type = RT_FALSE;
        basetype = RTB_CONST;
        return;
    }
    if(str == "NULL")
    {
        type = RT_NULL;
        basetype = RTB_CONST;
        return;
    }

    if(str == "+")
    {
        type = RT_PLUS;
        basetype = RTB_OP;
        priority = ROP_PLUSMINUS;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == "-")
    {
        type = RT_MINUS;
        basetype = RTB_OP;
        priority = ROP_PLUSMINUS;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == "*")
    {
        type = RT_MUL;
        basetype = RTB_OP;
        priority = ROP_MULDIV;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == "/")
    {
        type = RT_DIV;
        basetype = RTB_OP;
        priority = ROP_MULDIV;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }

    if(str == "!")
    {
        type = RT_NOT;
        basetype = RTB_OP;
        priority = ROP_NOT;
        argsExpected = 1;
        isLeftAssociated = false;
        return;
    }
    if(str == "&")
    {
        type = RT_AND;
        basetype = RTB_OP;
        priority = ROP_ANDOR;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == "|")
    {
        type = RT_OR;
        basetype = RTB_OP;
        priority = ROP_ANDOR;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }

    if(str == "==")
    {
        type = RT_EQ;
        basetype = RTB_OP;
        priority = ROP_CMP;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == "!=")
    {
        type = RT_NE;
        basetype = RTB_OP;
        priority = ROP_CMP;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == "<")
    {
        type = RT_LT;
        basetype = RTB_OP;
        priority = ROP_CMP;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == ">")
    {
        type = RT_GT;
        basetype = RTB_OP;
        priority = ROP_CMP;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == "<=")
    {
        type = RT_LE;
        basetype = RTB_OP;
        priority = ROP_CMP;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }
    if(str == ">=")
    {
        type = RT_GE;
        basetype = RTB_OP;
        priority = ROP_CMP;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }

    if(str == "<-")
    {
        type = RT_ASSIGN;
        basetype = RTB_OP;
        priority = ROP_ASSIGN;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }

    if(str == "[]")
    {
        //std::cout << "RToken: creating [] operator token." << std::endl;
        type = RT_IDX;
        basetype = RTB_OP;
        //Приоритет не выставляем, потому что этот оператор всегда будет сразу складываться в очередь при обработке квадратных скобок в стеке
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }

    if(str == "[]<-")
    {
        type = RT_IDX_ASSIGN;
        basetype = RTB_OP;
        priority = ROP_ASSIGN;
        argsExpected = 3;
        isLeftAssociated = true;
        return;
    }

    if(str == ":")
    {
        type = RT_RANGE;
        basetype = RTB_OP;
        priority = ROP_RANGE;
        argsExpected = 2;
        isLeftAssociated = true;
        return;
    }

    if(str == "(")
    {
        type = RT_BR_OP;
        basetype = RTB_BRACKET;
        return;
    }
    if(str == ")")
    {
        type = RT_BR_CL;
        basetype = RTB_BRACKET;
        return;
    }
    if(str == "[")
    {
        type = RT_IDX_OP;
        basetype = RTB_BRACKET;
        return;
    }
    if(str == "]")
    {
        type = RT_IDX_CL;
        basetype = RTB_BRACKET;
        return;
    }
    if(str == "{")
    {
        type = RT_BLOCK_OP;
        basetype = RTB_BRACKET;
        return;
    }
    if(str == "}")
    {
        type = RT_BLOCK_CL;
        basetype = RTB_BRACKET;
        return;
    }

    if(str == "repeat")
    {
        type = RT_REPEAT;
        basetype = RTB_RWORD;
        return;
    }
    if(str == "break")
    {
        type = RT_BREAK;
        basetype = RTB_RWORD;
        return;
    }
    if(str == "if")
    {
        type = RT_IF;
        basetype = RTB_RWORD;
        return;
    }
    if(str == "else")
    {
        type = RT_ELSE;
        basetype = RTB_RWORD;
        return;
    }

    if(str == ",")
    {
        type = RT_COMMA;
        basetype = RTB_COMMA;
        return;
    }

    std::vector<std::string> &names = RInt::getInstance().funcNames;
    int l = names.size(); //проверим, является ли именем функции
    for(int i = 0; i < l; ++i)
    {
        if(names[i] == str)
        {
            type = RT_FUNC;
            basetype = RTB_FUNC;
            return;
        }
    }

    bool alpha = false;
    bool digit = false;
    int point = 0;
    int len = str.length();
    for(int i = 0; i < len; ++i) //пытаемся понять, число или строка
    {
        alpha |= isalpha(str[i]);
        digit |= isdigit(str[i]);
        point += (str[i] == '.'); //в числе не может быть больше одной точки
    }

    if(alpha)
    {
        type = RT_ID;
        basetype = RTB_ID;
        return;
    }
    else
    {
        if(point <= 1)
        {
            //std::cout << "RToken: creating numeric constant " << data << " (" << RT_RNUM << ")" << std::endl;
            type = RT_RNUM;
            basetype = RTB_CONST;
            return;
        }
        else
        {
            std::cerr << RERR_UNEXP_TOKEN << " Unexpected format of numeric constant: " << str << std::endl;
            exit(1);
        }
    }

    std::cerr << RERR_UNEXP_TOKEN << " Unexpected token: " << str << std::endl;
    exit(1);

}

RToken::RToken(const RVec& _vector)
{
    type = RT_VEC;
    basetype = RTB_CONST;
    vector = _vector;
}

const RVec RToken::getVector() const throw(UndefVarException)
{
    assert(basetype == RTB_CONST || basetype == RTB_ID); //чтобы не вызывать вектор, от чего не следует
    switch(type)
    {
        case RT_RNUM:
            return RVec(RNum(data));
        case RT_RCHAR:
            return RVec(data);
        case RT_TRUE:
            return RVec(true);
        case RT_FALSE:
            return RVec(false);
        case RT_NULL:
            return RVec(RTYPE_NULL, 1);
        case RT_VEC:
            return vector;
        default:                                     //case RT_ID:
            return RInt::getInstance().getVar(data); //если передали идентификатор, то должны найти переменную, соответствующую ему;
    }
}

RVar *RToken::getRVar() const throw(RTypeException) //из tokenа получить Rvar, для одного элемента
{
    switch(type)
    {
    case RT_RNUM:
        //std::cout << "RToken: creating RNum with " << data << std::endl;
        return new RNum(data);
    case RT_RCHAR:
        return new RChar(data);
    case RT_TRUE:
        return new RLog(true);
    case RT_FALSE:
        return new RLog(false);
    case RT_NULL:
        return new RVar();
    default:
        throw RTypeException();
    }
}
