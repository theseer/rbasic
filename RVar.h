#ifndef RVAR_H
#define RVAR_H

#include <string>
#include <sstream>

enum rtype{RTYPE_NULL, RTYPE_NUM, RTYPE_CHAR, RTYPE_LOG};

class RTypeException : public std::exception{}; //если например перемножаем не то, что нужно
class NullTypeException : public RTypeException{}; //отслеживание операторов с нулум

class RNum;
class RChar;
class RLog; //логические булевые значения

class RVar
{
private:

public:
	RVar();
    RVar(const RVar&);
    virtual ~RVar();
	
    RVar& operator=(const RVar&);
	
    virtual const std::string toString() const; //печать переменных в строково виде
    RVar *cvtType(const rtype) const throw(RTypeException); //для приведения одного типа к другому
	virtual const RNum toNum() const throw(RTypeException); //для приведения к RNum
	virtual const RChar toChar() const throw(RTypeException);
	virtual const RLog toLog() const throw(RTypeException);
	
	virtual const RNum operator-() const throw(RTypeException);
	virtual const RLog operator~() const throw(RTypeException);
	
	virtual const RLog operator&(const RVar&) const throw(RTypeException);
	virtual const RLog operator|(const RVar&) const throw(RTypeException);
	
	virtual const RNum operator+(const RVar&) const throw(RTypeException);
	virtual const RNum operator-(const RVar&) const throw(RTypeException);
	virtual const RNum operator*(const RVar&) const throw(RTypeException);
    virtual const RNum operator/(const RVar&) const throw(RTypeException);
	
	virtual const RLog operator==(const RVar&) const throw(RTypeException);
	virtual const RLog operator!=(const RVar&) const throw(RTypeException);
	virtual const RLog operator>(const RVar&) const throw(RTypeException);
	virtual const RLog operator>=(const RVar&) const throw(RTypeException);
	virtual const RLog operator<(const RVar&) const throw(RTypeException);
	virtual const RLog operator<=(const RVar&) const throw(RTypeException);
	
	RVar& operator+=(const RVar&) throw(RTypeException);
	RVar& operator-=(const RVar&) throw(RTypeException);
	RVar& operator*=(const RVar&) throw(RTypeException);
	RVar& operator/=(const RVar&) throw(RTypeException);
	RVar& operator&=(const RVar&) throw(RTypeException);
	RVar& operator|=(const RVar&) throw(RTypeException);	
	
    virtual bool isNull() const; //возвращает тру фолс, если нулл или не нулл, родовой класс вернет тру, наследники фолс
};

class RNum : public RVar
{
private:
	double value;
	
public:
	RNum();
    RNum(const RNum&);
    RNum(const double);
    RNum(const std::string); //Не проверяет строку на валидность, использовать осторожно
    virtual ~RNum();

    RNum& operator=(const RNum&);

    virtual const std::string toString() const; //печать переменных в строково виде
    virtual const RNum toNum() const throw(RTypeException); //для приведения к RNum
    virtual const RChar toChar() const throw(RTypeException);
    virtual const RLog toLog() const throw(RTypeException);

    virtual const RNum operator-() const throw(RTypeException);
    virtual const RLog operator~() const throw(RTypeException);

    virtual const RLog operator&(const RVar&) const throw(RTypeException);
    virtual const RLog operator|(const RVar&) const throw(RTypeException);

    virtual const RNum operator+(const RVar&) const throw(RTypeException);
    virtual const RNum operator-(const RVar&) const throw(RTypeException);
    virtual const RNum operator*(const RVar&) const throw(RTypeException);
    virtual const RNum operator/(const RVar&) const throw(RTypeException);

    virtual const RLog operator==(const RVar&) const throw(RTypeException);
    virtual const RLog operator!=(const RVar&) const throw(RTypeException);
    virtual const RLog operator>(const RVar&) const throw(RTypeException);
    virtual const RLog operator>=(const RVar&) const throw(RTypeException);
    virtual const RLog operator<(const RVar&) const throw(RTypeException);
    virtual const RLog operator<=(const RVar&) const throw(RTypeException);

	bool isInteger() const; //все операции получают Rvar, Rvar => Rnum
    bool isNatural() const;
	double getValue() const;
    virtual bool isNull() const;
};

class RChar : public RVar
{
private:
	std::string value;
	
public:
	RChar();
	RChar(const RChar&);
	RChar(const std::string);
	RChar(const char*);
    virtual ~RChar();
	
    RChar& operator=(const RChar&);

    virtual const std::string toString() const; //печать переменных в строково виде
    virtual const RNum toNum() const throw(RTypeException); //для приведения к RNum
    virtual const RChar toChar() const throw(RTypeException);
    virtual const RLog toLog() const throw(RTypeException);

    virtual const RNum operator-() const throw(RTypeException);
    virtual const RLog operator~() const throw(RTypeException);

    virtual const RLog operator&(const RVar&) const throw(RTypeException);
    virtual const RLog operator|(const RVar&) const throw(RTypeException);

    virtual const RNum operator+(const RVar&) const throw(RTypeException);
    virtual const RNum operator-(const RVar&) const throw(RTypeException);
    virtual const RNum operator*(const RVar&) const throw(RTypeException);
    virtual const RNum operator/(const RVar&) const throw(RTypeException);

    virtual const RLog operator==(const RVar&) const throw(RTypeException);
    virtual const RLog operator!=(const RVar&) const throw(RTypeException);
    virtual const RLog operator>(const RVar&) const throw(RTypeException);
    virtual const RLog operator>=(const RVar&) const throw(RTypeException);
    virtual const RLog operator<(const RVar&) const throw(RTypeException);
    virtual const RLog operator<=(const RVar&) const throw(RTypeException);
	
    virtual bool isNull() const;
};

class RLog : public RVar
{
private:
	bool value;
	
public:
	RLog();
	RLog(const RLog&);
	RLog(const bool);
    virtual ~RLog();
	
    RLog& operator=(const RLog&);

    virtual const std::string toString() const; //печать переменных в строково виде
    virtual const RNum toNum() const throw(RTypeException); //для приведения к RNum
    virtual const RChar toChar() const throw(RTypeException);
    virtual const RLog toLog() const throw(RTypeException);

    virtual const RNum operator-() const throw(RTypeException);
    virtual const RLog operator~() const throw(RTypeException);

    virtual const RLog operator&(const RVar&) const throw(RTypeException);
    virtual const RLog operator|(const RVar&) const throw(RTypeException);

    virtual const RNum operator+(const RVar&) const throw(RTypeException);
    virtual const RNum operator-(const RVar&) const throw(RTypeException);
    virtual const RNum operator*(const RVar&) const throw(RTypeException);
    virtual const RNum operator/(const RVar&) const throw(RTypeException);

    virtual const RLog operator==(const RVar&) const throw(RTypeException);
    virtual const RLog operator!=(const RVar&) const throw(RTypeException);
    virtual const RLog operator>(const RVar&) const throw(RTypeException);
    virtual const RLog operator>=(const RVar&) const throw(RTypeException);
    virtual const RLog operator<(const RVar&) const throw(RTypeException);
    virtual const RLog operator<=(const RVar&) const throw(RTypeException);
	
    virtual bool isNull() const;
    bool getValue() const;
};

rtype resolveType(rtype, rtype);

#endif
