﻿#include "RVec.h"
#include <iostream>

void RVec::init(const std::vector<RVar*>& vec, rtype _type)
{
    type = _type;
    int l = vec.size();
    for(int i = 0; i < l; ++i)
    {
        data.push_back(copyVar(vec[i]));
    }
}

RVar *RVec::copyVar(const RVar *var) const throw(RTypeException)
{
    if(var->isNull())
    {
        return new RVar();
    }
    switch(type)
    {
        case RTYPE_NUM:
            return new RNum(var->toNum());
        case RTYPE_CHAR:
            return new RChar(var->toChar());
        case RTYPE_LOG:
            return new RLog(var->toLog());
        default:
            return new RVar();
    }
}

void RVec::destroyData()
{
    int l = data.size();
    for(int i = 0; i < l; ++i)
    {
        delete data[i];
    }
    data.clear();
}

RVec::RVec() : type(RTYPE_NULL)
{
    //std::cout << "Called RVec default constructor" << std::endl;
}

RVec::RVec(const RVec& other)
{
    //std::cout << "Called RVec copy constructor" << std::endl;
    init(other.data, other.type);
}

RVec::RVec(const std::vector<double>& vec) : type(RTYPE_NUM)
{
	int s = vec.size();
	for(int i = 0; i < s; ++i)
	{
        data.push_back(new RNum(vec[i]));
	}
}

RVec::RVec(const std::vector<std::string>& vec) : type(RTYPE_CHAR)
{
	int s = vec.size();
	for(int i = 0; i < s; ++i)
	{
        data.push_back(new RChar(vec[i]));
	}	
}

RVec::RVec(const std::vector<bool>& vec) : type(RTYPE_LOG)
{
	int s = vec.size();
	for(int i = 0; i < s; ++i)
	{
        data.push_back(new RLog(vec[i]));
	}
}

RVec::RVec(const rtype _type, int _size) : type(_type)
{
    for(int i = 0; i < _size; ++i)
    {
        data.push_back(new RVar());
    }
}

RVec::RVec(const std::vector<RNum>& vec) : type(RTYPE_NUM)
{
    int l = vec.size();
    for(int i = 0; i < l; ++i)
    {
        data.push_back(new RNum(vec[i]));
    }
}

RVec::RVec(const std::vector<RChar>& vec) : type(RTYPE_CHAR)
{
    int l = vec.size();
    for(int i = 0; i < l; ++i)
    {
        data.push_back(new RChar(vec[i]));
    }
}

RVec::RVec(const std::vector<RLog>& vec) : type(RTYPE_LOG)
{
    int l = vec.size();
    for(int i = 0; i < l; ++i)
    {
        data.push_back(new RLog(vec[i]));
    }
}

RVec::RVec(const RNum& num) : type(RTYPE_NUM)
{
    data.push_back(new RNum(num));
    //std::cout << "RVec: created vector " << toString() << " from " << num.getValue() << std::endl;
}

RVec::RVec(const RLog& log) : type(RTYPE_LOG)
{
    data.push_back(new RLog(log));
}

RVec::RVec(const RChar& rchar) : type(RTYPE_CHAR)
{
    data.push_back(new RChar(rchar));
}

RVec::RVec(const double num) : type(RTYPE_NUM)
{
    data.push_back(new RNum(num));
}

RVec::RVec(const std::string& str) : type(RTYPE_CHAR)
{
    data.push_back(new RChar(str));
}

RVec::RVec(const bool b) : type(RTYPE_LOG)
{
    data.push_back(new RLog(b));
}

RVec::RVec(double a, double b) : type(RTYPE_NUM)
{
    if(a >= b)
	{
        for(; a >= b; --a)
		{
            data.push_back(new RNum(a));
		}
	}
	else
	{
        for(; a <= b; ++a)
		{
            data.push_back(new RNum(a));
		}
	}
}

RVec::~RVec()
{
    destroyData();
}

RVec& RVec::operator=(const RVec& other)
{
	if(this != &other)
    {
        destroyData();
        //std::cout << "RVec: data size after destruction: " << data.size() << std::endl;
        init(other.data, other.type);
	}
	return *this;
}

const RVec RVec::length() const
{
    return RVec(double(data.size()));
}

const RVec RVec::mode() const
{
	switch(type)
	{
		case RTYPE_NUM:
            return RVec(std::string("Numeric"));
		case RTYPE_CHAR:
            return RVec(std::string("Character"));
		case RTYPE_LOG:
            return RVec(std::string("Logical"));
		default:
            return RVec(std::string("NULL"));
	}
}

std::string RVec::toString() const //преобразуем вектор в строку
{
	int size = data.size();
	if(size == 0)
	{
		switch(type)
		{
			case RTYPE_NUM:
				return std::string("number(0)");
			case RTYPE_CHAR:
				return std::string("character(0)");
			case RTYPE_LOG:
				return std::string("logical(0)");
			default:
				return std::string("NULL(0)");
		}		
	}
	std::string str = std::string("");
	for(int i = 0; i < size; ++i)
	{
        str += data[i]->toString();
		if(i != size - 1)
		{
			str += " ";
		}
	}
	return str;	
}

const RVec RVec::operator-() const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s = data.size();
	for(int i = 0; i < s; ++i)
    {
        try
        {
            vec.push_back(new RNum(-(*data[i]))); //если Лог, то он преобразуется к даблу и обработается
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_NUM);
}

const RVec RVec::operator~() const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s = data.size();
	for(int i = 0; i < s; ++i)
    {
        try
        {
            vec.push_back(new RLog(~(*data[i])));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

const RVec RVec::operator&(const RVec& other) const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
    {
        try
        {
            vec.push_back(new RLog(*data[i % s1] & *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

const RVec RVec::operator|(const RVec& other) const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        RVar *ptr1 = data[i % s1];
        RVar *ptr2 = other.data[i % s2];
        try
        {
            if(ptr1->isNull()) //Чтобы TRUE | NULL вывело TRUE
            {
                vec.push_back(new RLog(ptr2->toLog()));
            }
            else if(ptr2->isNull())
            {
                vec.push_back(new RLog(ptr1->toLog()));
            }
            else
            {
                vec.push_back(new RLog(*ptr1 | *ptr2));
            }
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

const RVec RVec::operator+(const RVec& other) const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RNum(*data[i % s1] + *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_NUM);
}

const RVec RVec::operator-(const RVec& other) const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RNum(*data[i % s1] - *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_NUM);
}

const RVec RVec::operator*(const RVec& other) const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RNum(*data[i % s1] * *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_NUM);
}

const RVec RVec::operator/(const RVec& other) const throw(RTypeException)
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RNum(*data[i % s1] / *other.data[i % s2]));
        }
        catch(NullTypeException)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_NUM);
}

const RVec RVec::operator==(const RVec& other) const
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RLog(*data[i % s1] == *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

const RVec RVec::operator!=(const RVec& other) const
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RLog(*data[i % s1] != *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

const RVec RVec::operator>(const RVec& other) const
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RLog(*data[i % s1] > *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

const RVec RVec::operator>=(const RVec& other) const
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RLog(*data[i % s1] >= *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}
const RVec RVec::operator<(const RVec& other) const
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RLog(*data[i % s1] < *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

const RVec RVec::operator<=(const RVec& other) const
{
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = other.data.size();
	int m = std::max(s1, s2);
	for(int i = 0; i < m; ++i)
	{
        try
        {
            vec.push_back(new RLog(*data[i % s1] <= *other.data[i % s2]));
        }
        catch(const NullTypeException&)
        {
            vec.push_back(new RVar());
        }
	}
    return RVec(vec, RTYPE_LOG);
}

RVec& RVec::operator+=(const RVec& other) throw(RTypeException)
{
	return (*this = (*this + other));
}

RVec& RVec::operator-=(const RVec& other) throw(RTypeException)
{
	return (*this = (*this - other));
}

RVec& RVec::operator*=(const RVec& other) throw(RTypeException)
{
	return (*this = (*this * other));
}

RVec& RVec::operator/=(const RVec& other) throw(RTypeException)
{
	return (*this = (*this / other));
}

RVec& RVec::operator&=(const RVec& other) throw(RTypeException)
{
    return (*this = (*this & other));
}

RVec& RVec::operator|=(const RVec& other) throw(RTypeException)
{
    return (*this = (*this | other));
}

RVec RVec::operator[](const RVec& idx) const throw(RTypeException)
{
    //std::cout << "RVec: getting (" << toString() << ")[" << idx.toString() << "]." << std::endl;
	if(idx.type != RTYPE_LOG && idx.type != RTYPE_NUM)
	{
		throw RTypeException();
	}
    std::vector<RVar*> vec;
	int s1 = data.size();
	int s2 = idx.data.size();
	if(idx.type == RTYPE_LOG)
    {
		for(int i = 0; i < s1; ++i) //идем по нашему вектору
        {
            RLog l = idx.data[i % s2]->toLog(); //смотрим переданный вектор, если в нем тру, то "выдираем" символ; циклим, если s2 < s1
            if(l.getValue())
            {
                vec.push_back(copyVar(data[i]));
            }
		}
        for(int i = s1; i < s2; ++i) //Если логический вектор-индекс длиннее вектора значений
        {
            RLog l = idx.data[i % s2]->toLog(); //смотрим переданный вектор, если в нем тру, то "выдираем" символ; циклим, если s2 < s1
            if(l.getValue()) //Если встретили TRUE, складываем NULL в результирующий
            {
                vec.push_back(new RVar());
            }
        }
		return RVec(vec, type);
	}
	else  //idx.type == RTYPE_NUM
	{
        for(int i = 0; i < s2; ++i)
        {
            try
            {
                RNum n = idx.data[i]->toNum();
                if(!n.isNatural())
                {
                    throw RTypeException();
                }
                if(n.getValue() > s1)
                {
                    //std::cout << "RVec: pushing NULL to result of indexing (idx: " << n.toString() << " size: " << s1 << ")." << std::endl;
                    vec.push_back(new RVar()); //добавляем один Нулл
                }
                else
                {
                    vec.push_back(copyVar(data[(int)n.getValue() - 1]));
                }
            }
            catch(const NullTypeException&){} //игнорировать нулевый значения в векторе
        }
        RVec ret(vec, type);
        //std::cout << "RVec: returning " << ret.toString() << std::endl;
        return ret;
    }
}

RVec& RVec::setValue(const int idx, const RVec& other) throw(RTypeException)
{
	rtype newtype = resolveType(type, other.type); //определяем, к какому типу нужно преобразовать первый, чтобы сложить в него второй 
    if(type != newtype && type != RTYPE_NULL)
	{
		cvtType(newtype); //преобразуем вектор к другому типу
	}
    type = newtype;
    RVar *val;
	if(other.data.size() != 0)
	{
        val = copyVar(other.data[0]);
	}
    else
    {
        val = new RVar();
    }
	int l = data.size();
	if(idx > l)
	{
		for(int i = l; i < idx; ++i)
		{
            data.push_back(new RVar());
		}
		data.push_back(val);
	}
	else
	{
        delete data[idx];
		data[idx] = val;
	}
	return *this;
}

RVar *RVec::getValue(const int idx) const throw(NullTypeException)
{
    if(idx >= int(data.size()))
    {
        throw NullTypeException();
	}
    return data[idx];
}

void RVec::append(const RVar *elem) throw(RTypeException) //первый элемент вектора elem положить в конец
{
    //std::cout << "RVec: appending " << elem->toString() << std::endl;
    if(elem->isNull()) //если Rvar, то вернет 1, и положим без конвертирования
    {
        data.push_back(new RVar());
    }
    else
    {
        data.push_back(elem->cvtType(type)); //конвертация, cvtType возвращает указатель на новый элемент типа type, созданного с помощью new
    }
}

RVec& RVec::cvtType(const rtype newtype) throw(RTypeException)
{
	if(type == newtype)
	{
        return *this;
	}
    type = newtype;
    std::vector<RVar*> vec; //лучше создать вектор, так как если будут исключения при изменении текущего вектора, это может "сломать" объект
    int l = data.size();
	for(int i = 0; i < l; ++i)
	{
        vec.push_back(copyVar(data[i]->cvtType(newtype)));
    }
    destroyData(); //Очищаем от старых значений
	data = vec;
	return *this;
}
