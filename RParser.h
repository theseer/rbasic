#ifndef RPARSER_H
#define RPARSER_H

#include "RToken.h"

#include <stack>
#include <queue>
#include <string>
#include <cstdio>
#include <cctype>
#include <iostream>

//Разбивает входящий текст на лексемы, создает токен, ложет в очереди, дойдет до ";", алгоритм сортировочной станции

class RInt;

enum char_type
{	RC_NORMAL, //цифры, точка, подчеркивание
	RC_TERM, //+,-,=, *, >=, <, ...
	RC_SPACE,//пробел
	RC_ENDL,//endl, ;
	RC_COMMENT, /// 
    RC_BRACKET, //(), {}, []
    RC_QUOT, //""
	RC_COMMA, //,
	RC_EOF, //EOF 
	RC_BAD}; //недопустимые символы
//разные типы одиночных символов

class RParser
{
private:
    bool interactive; //если выражение не кончилось, то на новой строке вывести "+" на новой строке, нужно будет выводить "+"
    std::queue<RToken> inQueue; //входная очередь
    std::queue<RToken> &outQueue; //выходная
    std::stack<RToken> stack; //используем в процессе
    std::istream& in; //потоки
    std::ostream& out;
    std::ostream& err;

    bool flush(std::string&, const bool=false); //из строки сделать токен и засунуть его в очередь
    bool flush(const char); //на случай, если, например, один символ будет
    void checkIOErr();  //проверка ошибок ввода и вывода

    bool runQueue(); //запуск сортировки, из него вызовем интерпретатор

    bool isLastArg;

public:
    RParser(bool _interactive=false, std::istream& _in=std::cin, std::ostream& _out=std::cout, std::ostream& _err=std::cerr);
    ~RParser(){}

    void parse(); //парсим

};

static const std::string termChars = std::string("+-*/=<>&|!:");
static const std::string brackets = std::string("()[]{}");
static const std::string singleTermChars = std::string("+-*/&|:");
static const std::string doubleTermChars = std::string("<= >= == != <-"); //< и > лежат как <= >=
#endif
