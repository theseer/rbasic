#include "RParser.h"
#include "RInt.h"

static char_type getType(int c) //возвращает тип символа с "Нашей" точки зрения
{
    if(isalnum(c) || c == '.' || c == '_')
    {
        return RC_NORMAL;
    }

    if(termChars.find(c) != std::string::npos)
    {
        return RC_TERM;
    }

    if(c == '\n' || c == ';')
    {
        return RC_ENDL;
    }

    if(isspace(c))
    {
        return RC_SPACE;
    }

    if(c == '#')
    {
        return RC_COMMENT;
    }

    if(brackets.find(c) != std::string::npos)
    {
        return RC_BRACKET;
    }

    if(c == '\"')
    {
        return RC_QUOT;
    }

    if(c == ',')
    {
        return RC_COMMA;
    }

    if(c == EOF)
    {
        return RC_EOF;
    }

    return RC_BAD;
}

void RParser::parse()
{
    if(interactive)
    {
        out << "> ";
    }

    std::string buff; //строка, которая для одной лексемы
    bool term = false;
    bool quot;
    while(!in.eof())
    {
        int c = in.get(); //получили символ из потока ввода
        char_type type = getType(c); //получаем тип символа пришедшего
        switch(type)
        {
        case RC_NORMAL:
            if(term) //были ли перед символом терминальные символы
            {
                flush(buff); //лексема готова, преобразуем ее в токен и складываем в очередь
                term = false;
            }
            buff.push_back(c);
            break;
        case RC_TERM:
            if(!term)
            {
                flush(buff); //не было терминалов, нужно разбивать на токены
                term = true;
            }
            if(buff.empty()) //true - если пустой
            {
                if(singleTermChars.find(c) != std::string::npos) //нашли среди "одно-символов"
                {
                    flush(c);
                }
                else
                {
                    buff.push_back(c);
                }
            }
            else
            {
                buff.push_back(c);
                if(doubleTermChars.find(buff) != std::string::npos) //найден среди "двух-символов", например, <=
                {
                    flush(buff);
                }
                else
                {
                    err << RERR_UNEXP_TOKEN << " Unexpected character sequence: " << buff << std::endl;
                    exit(1);
                }
            }
            break;
        case RC_SPACE:
            flush(buff);
            break;
        case RC_ENDL:
            flush(buff);
            if(runQueue())
            {
                if(interactive)
                {
                    out << "> ";
                }
                isLastArg = false;
            }
            else
            {
                if(interactive)
                {
                    out << "+ ";
                }
            }
            break;
        case RC_COMMENT: //#
            flush(buff);
            while(true) //считываем, пока не дойдем до перевода строки или до ЕОФ
            {
                char ch = in.peek();
                if(ch == EOF)
                {
                    checkIOErr(); //не случилось ли ошибки ввода - вывода
                    break;
                }
                if(ch == '\n')
                {
                    break;
                }
                in.get();
            }
            break;
        case RC_BRACKET:
            flush(buff);
            flush(c);
            break;
        case RC_QUOT:
            if(!term && !buff.empty()) //не было терминалов и не пустой, значит там были буквы, не может быть abc"a
            {
                err << RERR_UNEXP_TOKEN << " Unexpected characters sequence: " << buff << c << std::endl;
                exit(1);
            }

            flush(buff); //если +", то ошибка обнаружится при попытке прибавить строку, т.е. позже

            quot = true; //флаг для первой кавычки
            while(quot) //считываем, пока не встретим вторую кавычку
            {
                int ch = in.get();
                switch(ch)
                {
                    case EOF: //Файл кончился, а парной кавычки так и не встретилось
                        checkIOErr();
                        err << RERR_UNEXP_TOKEN << " Unexpected end of file after sequence: " << buff << ch << std::endl;
                        exit(1);
                    case '\\': //Обрабатываем Esc-последовательности
                    {
                        int next = in.get();
                        switch(next)
                        {
                            case EOF:
                                checkIOErr();
                                err << RERR_UNEXP_TOKEN << " Unexpected end of file after sequence: " << buff << '\\' << std::endl;
                                exit(1);
                            case '\"':
                                buff.push_back('\"');
                                break;
                            case 'n':
                                buff.push_back('\n');
                                break;
                            case 't':
                                buff.push_back('\t');
                                break;
                            case '\\':
                                buff.push_back('\\');
                                break;
                        }
                        break;
                    }
                    case '\"':
                        quot = false;
                        break;
                    default: //Если обычный символ, просто складываем его в буфер
                        buff.push_back(ch);
                }
            }
            flush(buff, true);
            break;
        case RC_COMMA: //, => аргументы функции
            flush(buff);
            flush(c);
            break;
        case RC_EOF:
            checkIOErr();
            runQueue();
            break;
        case RC_BAD:
            err << RERR_UNEXP_TOKEN << " Unexpected character: " << (char)c << std::endl;
            exit(1);
        }
    }
    if(!inQueue.empty())
    {
        runQueue();
    }
}


bool RParser::flush(std::string& buff, bool plainString) //Второй аргумент указывает, является ли строка обычной строковой константой
{
    if(buff.empty())
    {
        return false;
    }
    //out << "RParser: flushing " << buff << std::endl;

    RToken tk(buff, plainString); //Создаём из буфера токен

    if((tk.type == RT_MINUS || tk.type == RT_PLUS) && !isLastArg) //Если минус или плюс унарный
    {
        tk.argsExpected = 1; //Принимает один аргумент
        tk.isLeftAssociated = false; //Право-ассоциативный
        tk.priority = ROP_NOT; //Приоритет такой же, как у ~
    }

    if(tk.basetype == RTB_ID || tk.basetype == RTB_CONST || tk.type == RT_IDX_CL || tk.type == RT_BR_CL) //Если токен может быть левым операндом операции
    {
        isLastArg = true;
    }
    else
    {
        isLastArg = false;
    }

    inQueue.push(tk); //складываем в очередь
    buff.clear();
    return true;
}

bool RParser::flush(const char ch)
{
    std::string str(1, ch);
    return flush(str);
}

void RParser::checkIOErr()
{
    if(in.bad() || out.bad() || err.bad()) //проверим, есть ли ошибка в потоках
    {
        err << RERR_IO << " I/O error." << std::endl;
        exit(1);
    }
}

bool RParser::runQueue() //начало сортировочной станции
{
    while(!inQueue.empty())
    {
        RToken tk = inQueue.front(); //берем первый токен из очереди
        inQueue.pop(); //удаляем первый токен из очереди

        if(!stack.empty()) //Если в скобках есть хоть что-то, увеличиваем число аргументов до 1
        {
            RToken ttk = stack.top();
            if(ttk.type == RT_BR_OP && ttk.argsExpected == 0)
            {
                ttk.argsExpected = 1;
            }
        }

        //out << "RParser: took " << tk.data << " (type: " << tk.type << ") from inQueue." << std::endl;

        switch(tk.basetype) 
        {
            case RTB_ID: //имя переменной
                outQueue.push(tk); //кладём в очередь
                break;
            case RTB_CONST:
                outQueue.push(tk);
                break;
            case RTB_FUNC:
                stack.push(tk); //функция, кладём ее на "временный" стек
                if(!inQueue.empty())
                {
                    if(inQueue.front().type != RT_BR_OP)
                    {
                        err << RERR_UNEXP_TOKEN << " Unexpected token \'" << inQueue.front().data << "\' after function \'" << tk.data << "\'." << std::endl;
                        exit(1);
                    }
                }
                break;
            case RTB_COMMA:
            {
                while(true)
                {
                    if(stack.empty()) //не было открывающей скобки
                    {
                        err << RERR_UNEXP_TOKEN << " \',\'" << std::endl;
                        exit(1);
                    }
                    if(stack.top().type == RT_BR_OP) //нашли открывающую скобку, выходим
                    {
                        ++(stack.top().argsExpected);
                        break;
                    }
                    outQueue.push(stack.top()); //складываем в выходную очередь верхний элемент, например, могло быть f(2+2), тогда наверху увидим +, который должен сначала отработать
                    stack.pop();
                }
                break;
            }
            case RTB_OP: //ВСЕ операторы
                while(true) //Пока стек не пустой, идем
                {
                    if(stack.empty())
                    {
                        if(inQueue.empty())
                        {
                            stack.push(tk);
                            return false;
                        }
                        break;
                    }
                    if(stack.top().basetype == RTB_OP)
                    {
                        if((tk.isLeftAssociated && tk.priority <= stack.top().priority) ||
                                (!tk.isLeftAssociated && tk.priority < stack.top().priority)) //кладем тот оператор, который должен исполняться раньше
                        {
                            outQueue.push(stack.top());
                            stack.pop();
                            continue;
                        }
                    }
                    if(inQueue.empty())
                    {
                        stack.push(tk);
                        return false;
                    }
                    break;
                }
                stack.push(tk);
                break;
             case RTB_BRACKET:
                switch(tk.type)
                {
                    case RT_BR_OP:
                        if(inQueue.front().type == RT_BR_CL)
                        {
                            tk.argsExpected = 0;
                        }
                        else
                        {
                            tk.argsExpected = 1;
                        }
                        stack.push(tk);
                        break;
                    case RT_BR_CL:
                    {
                        while(true)
                        {
                            if(stack.empty()) //значит, не было открывающей скобки
                            {
                                err << RERR_UNEXP_TOKEN << " \')\'" << std::endl;
                                exit(1);
                            }
                            if(stack.top().type == RT_BR_OP) //если наверху откр. скобка
                            {
                                int args_total = stack.top().argsExpected;
                                stack.pop(); //выкидываем ее
                                if(!stack.empty())
                                {
                                    RToken f = stack.top();
                                    if(f.type == RT_FUNC) //если увидели имя функции
                                    {
                                        f.argsExpected = args_total;
                                        outQueue.push(f);
                                        stack.pop();
                                    }
                                }
								break;
                            }
                            if(stack.top().type == RT_IDX_CL)
                            {
                                while(true)
                                {
                                    if(stack.empty()) //значит, не было открывающей скобки
                                    {
                                        err << RERR_UNEXP_TOKEN << " \']\'" << std::endl;
                                        exit(1);
                                    }
                                    if(stack.top().type == RT_IDX_OP) //если наверху откр. скобка
                                    {
                                        stack.pop(); //выкидываем ее
                                        outQueue.push(RToken("[]", false)); //Складываем в очередь оператор взятия индекса
                                        break;
                                    }
                                    outQueue.push(stack.top()); //кладём все, что между скобками
                                    stack.pop();
                                }
                            }
                            outQueue.push(stack.top()); //кладём все, что между скобками
                            stack.pop();
                        }
                        break;
                    }
                    case RT_IDX_OP:
                        stack.push(tk);
                        break;
                    case RT_IDX_CL:
                        while(true)
                        {
                            if(stack.empty()) //значит, не было открывающей скобки
                            {
                                err << RERR_UNEXP_TOKEN << " Unexpected token \']\'" << std::endl;
                                exit(1);
                            }
                            if(stack.top().type == RT_IDX_OP) //если наверху откр. скобка
                            {
                                stack.pop(); //выкидываем ее и выходим из цикла
                                break;
                            }
                            outQueue.push(stack.top()); //кладём все, что между скобками
                            stack.pop();
                        }
                        if(!inQueue.empty())
                        {
                            if(inQueue.front().type == RT_ASSIGN)
                            {
                                stack.push(RToken("[]<-", false));
                                inQueue.pop();
                                break;
                            }
                        }
                        outQueue.push(RToken("[]", false));
                        break;
                    default:
                        break;
                }
                break;
            case RTB_RWORD:
                err << RERR_NOT_IMPLEMENTED << '\"' << tk.data << "\"is not implemented in this version." << std::endl;
                exit(1);
        }
    }
    while(!stack.empty())
    {
        if(stack.top().type == RT_BR_OP || stack.top().type == RT_FUNC || stack.top().type == RT_COMMA || stack.top().type == RT_IDX_OP) //Открывающая скобка или имя функции без скобки вообще
        {
            return false;
        }
        outQueue.push(stack.top());
        stack.pop();
    }
    std::string res = RInt::getInstance().runQueue();
    if(res != "")
    {
        //out << "Result: ";
        out << res << std::endl;
    }
    return true;
}

RParser::RParser(bool _interactive, std::istream &_in, std::ostream &_out, std::ostream &_err)
    : interactive(_interactive), outQueue(RInt::getInstance().getQueue()), in(_in), out(_out), err(_err), isLastArg(false)
{
}
