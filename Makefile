CFLAGS = -O2 -Wall
CC = g++ $(CFLAGS)

all: RBasic
	
RBasic: RBasic.o RVec.o RInt.o RParser.o RToken.o RVar.o
	$(CC) -o RBasic RBasic.o RVec.o RInt.o RParser.o RToken.o RVar.o

RBasic.o: RBasic.cpp RBasic.h
	$(CC) -c RBasic.cpp

RVec.o: RVec.cpp RVec.h RVar.h
	$(CC) -c RVec.cpp
	
RInt.o: RInt.cpp RInt.h RBasic.h RToken.h
	$(CC) -c RInt.cpp
	
RParser.o: RParser.cpp RParser.h RToken.h RInt.h
	$(CC) -c RParser.cpp
	
RToken.o: RToken.cpp RToken.h RVec.h RBasic.h
	$(CC) -c RToken.cpp

RVar.o: RVar.cpp RVar.h
	$(CC) -c RVar.cpp

clean:
	rm -rf *.o RBasic
