#include "RBasic.h"
#include "RParser.h"

#include <iostream>
#include <fstream>

static void printHelp(char *);
static void printHello();
 
int main(int argc, char **argv)
{
    switch(argc)
    {
        case 1:
        {
            printHello();
            RParser parser(true);
            parser.parse();
            return 0;
        }
        case 2:
        {
            std::fstream in(argv[1], std::ios_base::in);
            RParser parser(false, in);
            parser.parse();
            return 0;
        }
        default:
        {
            printHelp(argv[0]);
            return 0;
        }
    }
}

void printHello()
{
    std::cout << "RBasic by Artem Kozlov (2014, MSU)" << std::endl;
}

void printHelp(char *execName)
{
    std::cout << "Usage:" << std::endl
              << "\"" << execName << "\" for interactive interpretation" << std::endl
              << "\"" << execName << " script_name\" for script interpretation" << std::endl;
}
