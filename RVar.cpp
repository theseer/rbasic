#include "RVar.h"
#include <iostream>

rtype resolveType(rtype type1, rtype type2)  //принимает два типа, и решает к кому нужно привести
{	
	if(type1 == RTYPE_NULL || type2 == RTYPE_NULL) //если нулл, то приводить к нуллу
	{
		return RTYPE_NULL;
	}
	if(type1 == RTYPE_CHAR || type2 == RTYPE_CHAR) //если чар хотя бы один, то приводим к чару
	{
		return RTYPE_CHAR;
	}
	return type1; //иначе приводим к первому типу
}

RVar::RVar() {}
RVar::RVar(const RVar&) {}
RVar::~RVar() {}

RVar& RVar::operator=(const RVar&)
{
    return *this;
}

const std::string RVar::toString() const
{
    return std::string("NULL"); //Если создан как Rvar, то возвращаем Нулл
}

RVar *RVar::cvtType(const rtype type) const throw(RTypeException)
{
    //std::cout << "RVar: convert to " << type << std::endl;
    if(isNull())
    {
        return new RVar();
    }

    switch(type) //пытаемся сконвертировать тип
    {
        case RTYPE_NULL:
            return new RVar();
        case RTYPE_NUM:
            //std::cout << "Call RNum in cvtType" << std::endl;
            return new RNum(toNum());
        case RTYPE_CHAR:
            return new RChar(toChar());
        case RTYPE_LOG:
            return new RLog(toLog());
        default:
            throw RTypeException();
    }
}

const RNum RVar::toNum() const throw(RTypeException)
{
    throw NullTypeException(); // в Rvare toNum вызывается, значит ошибка
}

const RChar RVar::toChar() const throw(RTypeException)
{
    throw NullTypeException();
}

const RLog RVar::toLog() const throw(RTypeException)
{
    throw NullTypeException();
}

const RNum RVar::operator-() const throw(RTypeException)
{
	throw NullTypeException();
}

const RLog RVar::operator~() const throw(RTypeException)
{
	throw NullTypeException();
}

const RLog RVar::operator&(const RVar& other) const throw(RTypeException)
{
    other.toLog();
	throw NullTypeException();
}

const RLog RVar::operator|(const RVar& other) const throw(RTypeException)
{
    other.toLog();
	throw NullTypeException();
}

const RNum RVar::operator+(const RVar& other) const throw(RTypeException)
{
    other.toNum(); //сначала пытаемся преобразовать, чтобы в случае плохого типа вылетело RTypeException
	throw NullTypeException();
}

const RNum RVar::operator-(const RVar& other) const throw(RTypeException)
{
    other.toNum();
	throw NullTypeException();
}

const RNum RVar::operator*(const RVar& other) const throw(RTypeException)
{
    other.toNum();
	throw NullTypeException();
}

const RNum RVar::operator/(const RVar& other) const throw(RTypeException)
{
    other.toNum();
	throw NullTypeException();
}

const RLog RVar::operator==(const RVar&) const throw(RTypeException)
{
	throw NullTypeException();
}

const RLog RVar::operator!=(const RVar&) const throw(RTypeException)
{
	throw NullTypeException();
}

const RLog RVar::operator>(const RVar&) const throw(RTypeException)
{
	throw NullTypeException();
}

const RLog RVar::operator>=(const RVar&) const throw(RTypeException)
{
	throw NullTypeException();
}

const RLog RVar::operator<(const RVar&) const throw(RTypeException)
{
	throw NullTypeException();
}

const RLog RVar::operator<=(const RVar&) const throw(RTypeException)
{
	throw NullTypeException();
}

RVar& RVar::operator+=(const RVar& other) throw(RTypeException)
{
	*this = *this + other;
	return *this;
}

RVar& RVar::operator-=(const RVar& other) throw(RTypeException)
{
	*this = *this - other;
	return *this;
}

RVar& RVar::operator*=(const RVar& other) throw(RTypeException)
{
	*this = *this * other;
	return *this;
}

RVar& RVar::operator/=(const RVar& other) throw(RTypeException)
{
	*this = *this / other;
	return *this;
}

RVar& RVar::operator&=(const RVar& other) throw(RTypeException)
{
	*this = *this & other;
	return *this;
}

RVar& RVar::operator|=(const RVar& other) throw(RTypeException)
{
	*this = *this | other;
	return *this;
}

bool RVar::isNull() const
{
    return true;
}


RNum::RNum() : value(0) {}
RNum::RNum(const RNum& other) : RVar(other), value(other.value) {}
RNum::RNum(const double _value) : value(_value) {}

RNum::RNum(const std::string data)
{
    std::stringstream ss(data);
    ss >> value;
    //std::cout << "RNum: creating element with value " << value << std::endl;
}

RNum::~RNum() {}

RNum& RNum::operator=(const RNum& other)
{
	if(this != &other)
	{
		value = other.value;
	}
	return *this;
}

const std::string RNum::toString() const
{
    std::stringstream ss;
	if(isInteger()) //если число целое
	{
		ss << int(value);
	}
	else
	{
		ss << value;
    }
    //std::cout << "RNum: toString returned " << ss.str() << std::endl;
	return ss.str(); //чтобы распечатать вектор, и печать не зависела от типа
}

const RNum RNum::toNum() const throw(RTypeException)
{
    return *this;
}

const RChar RNum::toChar() const throw(RTypeException) //Rnum => Rchar, если например, сложить разные типы
{
    return RChar(toString());
}

const RLog RNum::toLog() const throw(RTypeException)
{
    return RLog(value != 0); //преобразование в булл
}

const RNum RNum::operator-() const throw(RTypeException)
{
    return RNum(-value);
}

const RLog RNum::operator~() const throw(RTypeException)
{
    return ~toLog(); //оператор отрицания
}

const RLog RNum::operator&(const RVar& other) const throw(RTypeException)
{
	return toLog() & other.toLog();
}

const RLog RNum::operator|(const RVar& other) const throw(RTypeException)
{
	return toLog() | other.toLog();
}

const RNum RNum::operator+(const RVar& other) const throw(RTypeException)
{
	return RNum(value + other.toNum().value);
}
const RNum RNum::operator-(const RVar& other) const throw(RTypeException)
{
	return RNum(value - other.toNum().value);	
}

const RNum RNum::operator*(const RVar& other) const throw(RTypeException)
{
	return RNum(value * other.toNum().value);	
}

const RNum RNum::operator/(const RVar& other) const throw(RTypeException)
{
	double otherval = other.toNum().value;
	if(otherval == 0)
	{
		throw NullTypeException();
	}		
	return RNum(value / otherval);	
}

const RLog RNum::operator==(const RVar& other) const throw(RTypeException)
{
    RNum othernum = other.toNum();
    return RLog(value == othernum.value);
}
const RLog RNum::operator!=(const RVar& other) const throw(RTypeException)
{
    RNum othernum = other.toNum();
    return RLog(value != othernum.value);
}

const RLog RNum::operator>(const RVar& other) const throw(RTypeException)
{
    RNum othernum = other.toNum();
    return RLog(value > othernum.value);
}

const RLog RNum::operator>=(const RVar& other) const throw(RTypeException)
{
	if(&other == this)
	{
		return RLog(true);
	}
    RNum othernum = other.toNum();
    return RLog(value >= othernum.value);
}

const RLog RNum::operator<(const RVar& other) const throw(RTypeException)
{
    RNum othernum = other.toNum();
    return RLog(value < othernum.value);
}

const RLog RNum::operator<=(const RVar& other) const throw(RTypeException)
{
    RNum othernum = other.toNum();
    return RLog(value <= othernum.value);
}

bool RNum::isInteger() const
{
	return (value - int(value) == 0);
}

bool RNum::isNatural() const
{
    return isInteger() && value > 0;
}

double RNum::getValue() const
{
    return value;
}

bool RNum::isNull() const
{
    return false;
}


RChar::RChar() : value(std::string("")){}
RChar::RChar(const RChar& other) : RVar(other), value(other.value){}
RChar::RChar(const std::string str) : value(str){}
RChar::RChar(const char* cstr) : value(std::string(cstr)){}
RChar::~RChar(){}

RChar& RChar::operator=(const RChar& other)
{
	if(this != &other)
	{
		value = other.value;
	}
	return *this;
}

const std::string RChar::toString() const
{
	return value;	
}

const RNum RChar::toNum() const throw(RTypeException)
{
	throw RTypeException(); //строку в число преобразовать не сможем
}

const RChar RChar::toChar() const throw(RTypeException)
{
    return *this;
}

const RLog RChar::toLog() const throw(RTypeException)
{
	throw RTypeException();	
}

const RNum RChar::operator-() const throw(RTypeException)
{
	throw RTypeException();
}

const RLog RChar::operator~() const throw(RTypeException)
{
	throw RTypeException();
}

const RLog RChar::operator&(const RVar&) const throw(RTypeException)
{
	throw RTypeException();
}

const RLog RChar::operator|(const RVar&) const throw(RTypeException)
{
	throw RTypeException();
}

const RNum RChar::operator+(const RVar&) const throw(RTypeException)
{
	throw RTypeException();
}

const RNum RChar::operator-(const RVar&) const throw(RTypeException)
{
	throw RTypeException();
}

const RNum RChar::operator*(const RVar&) const throw(RTypeException)
{
	throw RTypeException();
}

const RNum RChar::operator/(const RVar&) const throw(RTypeException)
{
	throw RTypeException();
}

const RLog RChar::operator==(const RVar& othervar) const throw(RTypeException)
{
    RChar other = othervar.toChar();
	return (value == other.value);
}

const RLog RChar::operator!=(const RVar& othervar) const throw(RTypeException)
{
    RChar other = othervar.toChar();
	return (value != other.value);
}

const RLog RChar::operator>(const RVar& othervar) const throw(RTypeException)
{
    RChar other = othervar.toChar();
	return (value > other.value);
}

const RLog RChar::operator>=(const RVar& othervar) const throw(RTypeException)
{
    RChar other = othervar.toChar();
	return (value >= other.value);
}

const RLog RChar::operator<(const RVar& othervar) const throw(RTypeException)
{
    RChar other = othervar.toChar();
	return (value < other.value);
}

const RLog RChar::operator<=(const RVar& othervar) const throw(RTypeException)
{
    RChar other = othervar.toChar();
	return (value <= other.value);
}

bool RChar::isNull() const
{
    return false;
}



RLog::RLog() : value(false){}
RLog::RLog(const RLog& other) : RVar(other), value(other.value){}
RLog::RLog(const bool bother) : value(bother){}
RLog::~RLog(){}

RLog& RLog::operator=(const RLog& other)
{
	if(this != &other)
	{
		value = other.value;
	}
    return *this;
}

const std::string RLog::toString() const
{
    return std::string(value ? "TRUE" : "FALSE");
}

const RNum RLog::toNum() const throw(RTypeException)
{
	return RNum(value ? 1 : 0);
}
const RChar RLog::toChar() const throw(RTypeException)
{
	return RChar(toString());
}

const RLog RLog::toLog() const throw(RTypeException)
{
	return *this;
}

const RNum RLog::operator-() const throw(RTypeException)
{
	return -toNum();
}

const RLog RLog::operator~() const throw(RTypeException)
{
	return RLog(~value);
}

const RLog RLog::operator&(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value & ovalue);
}

const RLog RLog::operator|(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value | ovalue);
}

const RNum RLog::operator+(const RVar& other) const throw(RTypeException)
{
	return toNum() + other.toNum();
}
const RNum RLog::operator-(const RVar& other) const throw(RTypeException)
{
	return toNum() - other.toNum();
}

const RNum RLog::operator*(const RVar& other) const throw(RTypeException)
{
	return toNum() * other.toNum();
}

const RNum RLog::operator/(const RVar& other) const throw(RTypeException)
{
	return toNum() / other.toNum();
}

const RLog RLog::operator==(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value == ovalue);
}

const RLog RLog::operator!=(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value != ovalue);
}

const RLog RLog::operator>(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value > ovalue);
}

const RLog RLog::operator>=(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value >= ovalue);
}

const RLog RLog::operator<(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value < ovalue);
}

const RLog RLog::operator<=(const RVar& other) const throw(RTypeException)
{
    bool ovalue = other.toLog().value;
	return RLog(value <= ovalue);
}

bool RLog::isNull() const
{
    return false;
}

bool RLog::getValue() const
{
    return value;
}


const RVar createRVar(double n)
{
    return RNum(n);
}


const RVar createRVar(std::string s)
{
    return RChar(s);
}


const RVar createRVar(bool b)
{
    return RLog(b);
}


const RVar createRVar() //Null
{
    return RVar();
}
