﻿#include "RInt.h"

RToken RFuncOp::c(const std::vector<RToken>& args)
{
    int s = args.size();
    //std::cout << "c(): took " << s << " args." << std::endl;
    if(s == 0)
    {
        return RToken(RVec());
    }
    RVec v;
    try
    {
        v = args[0].getVector();

        for(int i = 1; i < s; ++i)
        {
            v.append(args[i].getVector().getValue(0));
        }
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator c (all should has the same (except NULL) type)." << std::endl;
        exit(1);
    }
    //std::cout << "c(): Created vector " << v.toString() << " with type " << v.mode().toString() << std::endl;
    return RToken(v);
}

RToken RFuncOp::length(const std::vector<RToken>& args)
{
    assert(args.size() == 1); //сюда должен быть передан только один аргумент
    RToken tk = args[0];
    RVec v;
    if(tk.type == RT_VEC)
    {
        v = tk.vector;
    }
    else if(tk.type == RT_ID) //тогда нужно по имени переменной получить нужный нам вектор
    {
        if(RInt::getInstance().vars.count(tk.data) == 0) //если не нашли переменной с таким именем
        {
            std::cerr << RERR_UNDEF_VAR << " Variable \'" << tk.data << "\' is not defined." << std::endl;
            exit(1);
        }
        v = RInt::getInstance().vars[tk.data];
    }
    else
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for function \'length\' (expected vector)." << std::endl;
        exit(1);
    }
    return(RToken(v.length())); //создасть новый вектор из токенов
}

RToken RFuncOp::mode(const std::vector<RToken>& args) //возвращает вектор из строки, содержащей тип вектора
{
    assert(args.size() == 1);
    if(args[0].type != RT_VEC && args[0].type != RT_ID)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for function \'mode\' (expected vector)." << std::endl;
        exit(1);
    }
    RVec v;
    try
    {
        v = args[0].getVector();
    }
    catch(UndefVarException)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << args[0].data << "\' is not defined." << std::endl;
        exit(1);
    }
    return(RToken(v.mode()));
}

RToken RFuncOp::plus(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 1 || s == 2);
    try
    {
        if(s == 1)
        {
            return RToken(args[0].getVector());
        }
        RVec ret = args[0].getVector() + args[1].getVector();
        //std::cout << "RInt: " << args[0].getVector().toString() << " + " << args[1].getVector().toString() << " = " << ret.toString() << std::endl;
        return RToken(ret);
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        //std::cout << "NTE in plus" << std::endl;
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'+\' (expected vector)." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::minus(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 1 || s == 2);
    try
    {
        if(s == 1)
        {
            return RToken(-(args[0].getVector()));
        }
        return RToken(args[0].getVector() - args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'-\' (expected vector)." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::mul(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() * args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'*\' (expected vector)." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::div(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() / args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'/\' (expected vector)." << std::endl;
        exit(1);
    }

}

RToken RFuncOp::lnot(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 1);
    try
    {
        return RToken(~(args[0].getVector()));
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'!\' (expected vector)." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::land(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() & args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'&\' (expected vector)." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::lor(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() | args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'|\' (expected vector)." << std::endl;
        exit(1);
    }

}

RToken RFuncOp::eq(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() == args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'==\' (expected vector)." << std::endl;
        exit(1);
    }

}

RToken RFuncOp::ne(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() != args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'!=\' (expected vector)." << std::endl;
        exit(1);
    }

}

RToken RFuncOp::gt(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() > args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'>\' (expected vector)." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::lt(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() < args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'<\' (expected vector)." << std::endl;
        exit(1);
    }

}

RToken RFuncOp::ge(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() >= args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'>=\' (expected vector)." << std::endl;
        exit(1);
    }

}

RToken RFuncOp::le(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        return RToken(args[0].getVector() <= args[1].getVector());
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const NullTypeException&)
    {
        return RToken(RVec());
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong types of args for operator \'<=\' (expected vector)." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::range(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    try
    {
        double a = args[0].getVector().getValue(0)->toNum().getValue();
        double b = args[1].getVector().getValue(0)->toNum().getValue();
        return RToken(RVec(a, b));
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Wrong type of arguments for operator \':\''." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::assign(const std::vector<RToken> &args)
{
    int s = args.size();
    assert(s == 2);
    if(args[0].type != RT_ID)
    {
        std::cerr << RERR_BAD_TYPE << " Arguments of operation \'<-\' has bad types." << std::endl;
        exit(1);
    }
    try
    {
        RInt::getInstance().vars[args[0].data] = args[1].getVector();
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }

    return args[0]; //Возвращаем переменную, а не её значение
}

RToken RFuncOp::idx(const std::vector<RToken>& args)
{
    //std::cout << "idx() called with " << args[0].data << "[" << args[1].data << "]" << std::endl;
    int s = args.size();
    assert(s == 2);
    try
    {
        RVec v = args[0].getVector()[args[1].getVector()];
        //std::cout << "idx(): got " << std::endl;
        return RToken(v);
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
}

RToken RFuncOp::idxAssign(const std::vector<RToken>& args)
{
    int s = args.size();
    assert(s == 3);
    if(args[0].type != RT_ID)
    {
        std::cerr << RERR_BAD_TYPE << " Assignment to non-variable." << std::endl;
        exit(1);
    }
    try
    {
        RNum n = args[1].getVector().getValue(0)->toNum();
        if(!n.isNatural()) //Проверяем, что индекс натуральный
        {
            throw RTypeException();
        }
        if(RInt::getInstance().vars.count(args[0].data) == 0) //Проверяем, что переменная существует
        {
            throw UndefVarException(args[0].data);
        }
        //std::cout << "RInt: " << args[0].getVector().toString() << "[" << args[1].getVector().toString() << "] <- " << args[2].getVector().toString() << std::endl;
        RVec& var = RInt::getInstance().vars[args[0].data];
        var.setValue(n.getValue() - 1, args[2].getVector());

        return args[0];
    }
    catch(const UndefVarException& e)
    {
        std::cerr << RERR_UNDEF_VAR << " Variable \'" << e.message << "\' is not defined." << std::endl;
        exit(1);
    }
    catch(const RTypeException&)
    {
        std::cerr << RERR_BAD_TYPE << " Bad type for index argument." << std::endl;
        exit(1);
    }
}

void RInt::setQueue(const std::queue<RToken> &value) //проинициализировать очередь
{
    queue = value;
}

std::queue<RToken>& RInt::getQueue()
{
    return queue;
}

RInt::RInt()
{
    initFunctions();
    initOps();
}

std::string RInt::runQueue() //стек содержать будет операнды, будем идти по очереди и ложить операнды на стек, операции смотрим в очереди
{
    if(queue.size() == 1 && stack.empty()) //Если есть только одна переменная и ничего больше, печатаем её
    {
        RToken tk = queue.front();
        if(tk.type == RT_ID)
        {
            queue.pop();
            return tk.getVector().toString();
        }
    }
    stack = std::stack<RToken>();
    std::vector<RToken> args;
    while(!queue.empty())
    {
        RToken tk = queue.front();
        queue.pop();

        //std::cout << "RInt: Took " << tk.data << " (" << tk.type << ") from queue." << std::endl;

        switch(tk.basetype)
        {
            case RTB_ID:
                stack.push(tk);
                //std::cout << "RInt: Pushing " << tk.data << " in stack." << std::endl;
                break;
            case RTB_CONST:
                stack.push(tk);
                //std::cout << "RInt: Pushing " << tk.data << " in stack." << std::endl;
                break;
            case RTB_OP:
                args.clear();                
                args.resize(tk.argsExpected);
                for(int i = 0; i < tk.argsExpected; ++i)
                {
                    if(stack.empty())
                    {
                        std::cerr << RERR_BAD_ARGS << " Wrong number of args (" << i << ") for operation " << tk.data << " (expected " << tk.argsExpected << ")." << std::endl;
                        exit(1);
                    }
                    RToken arg = stack.top();
                    stack.pop();
                    if(arg.basetype != RTB_ID && arg.basetype != RTB_CONST)
                    {
                        std::cerr << RERR_BAD_ARGS << " Wrong number of args (" << i << ") for operation " << tk.data << " (expected " << tk.argsExpected << ")." << std::endl;
                        exit(1);
                    }
                    args[tk.argsExpected - i - 1] = arg;
                }                
                //std::cout << "RInt: calling operator " << tk.data << std::endl;
                stack.push((forCall.*(operators[tk.data]))(args)); //дата - имя оператора, положит результат его выполнения
                break;
            case RTB_FUNC:
                args.clear();
                args.resize(tk.argsExpected);
                for(int i = 0; i < tk.argsExpected; ++i)
                {
                    if(stack.empty())
                    {
                        std::cerr << RERR_BAD_ARGS << " Wrong number of args (" << i << ") for operation " << tk.data << " (expected " << tk.argsExpected << ")." << std::endl;
                        exit(1);
                    }
                    RToken arg = stack.top();
                    stack.pop();
                    if(arg.basetype != RTB_ID && arg.basetype != RTB_CONST)
                    {
                        std::cerr << arg.data << " (" << arg.type << ")" << std::endl;
                        std::cerr << RERR_BAD_ARGS << " Wrong number of args (" << i << ") for operation " << tk.data << " (expected " << tk.argsExpected << ")." << std::endl;
                        exit(1);
                    }
                    args[tk.argsExpected - i - 1] = arg;
                }
                //std::cout << "RInt: calling function " << tk.data << std::endl;
                stack.push((forCall.*functions[tk.data])(args));
                break;
            case RTB_RWORD:
                //TODO: RWORD
                break;
            default:
                break;
        }
    }
    if(!stack.empty())
    {
        RToken res = stack.top();
        stack.pop();
        if(res.type == RT_VEC)
        {
            //std::cout << "Returning " << std::endl;
            std::string ret = res.getVector().toString();
            return ret;
        }
    }
    return(std::string("")); //если не вернули хорошую строку
}

std::string RInt::runQueue(const std::queue<RToken> &runqueue)
{
    queue = runqueue;
    return runQueue();
}

const RVec RInt::getVar(const std::string &name) throw(UndefVarException)
{
    if(vars.count(name) == 0)
    {
        throw UndefVarException(name);
    }
    return vars[name];
}

void RInt::setVar(const std::string &name, const RVec &value)
{
    vars[name] = value;
}

void RInt::initFunctions()
{
    funcNames.push_back("c");
    functions["c"] = &RFuncOp::c;
    funcNames.push_back("length");
    functions["length"] = &RFuncOp::length;
    funcNames.push_back("mode");
    functions["mode"] = &RFuncOp::mode;
}

void RInt::initOps()
{
    operators["+"] = &RFuncOp::plus;
    operators["-"] = &RFuncOp::minus;
    operators["*"] = &RFuncOp::mul;
    operators["/"] = &RFuncOp::div;

    operators["!"] = &RFuncOp::lnot;
    operators["&"] = &RFuncOp::land;
    operators["|"] = &RFuncOp::lor;

    operators["=="] = &RFuncOp::eq;
    operators["!="] = &RFuncOp::ne;
    operators["<"] = &RFuncOp::lt;
    operators[">"] = &RFuncOp::gt;
    operators["<="] = &RFuncOp::le;
    operators[">="] = &RFuncOp::ge;

    operators[":"] = &RFuncOp::range;

    operators["<-"] = &RFuncOp::assign;
    operators["[]"] = &RFuncOp::idx;
    operators["[]<-"] = &RFuncOp::idxAssign;
}
