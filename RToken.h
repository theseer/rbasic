#ifndef RTOKEN_H //Класс лексемы
#define RTOKEN_H

#include "RVec.h"
#include "RBasic.h"

#include <string>
#include <exception>
#include <cctype>
#include <sstream>

class RInt;

enum rtoken_t
{
    RT_ID, //имя переменной
    RT_FUNC, //имя функции

    RT_RNUM, //число
    RT_RCHAR,//строка
    RT_VEC,//вектор

    RT_TRUE, //лог. значение тру
    RT_FALSE,//фолс
    RT_NULL,//нулл

    RT_PLUS,// +
    RT_MINUS,// -
    RT_MUL,// *
    RT_DIV,// /

    RT_NOT,// ~
    RT_AND,// &
    RT_OR,// |

    RT_EQ,// =
    RT_NE,// !=
    RT_GT,// >
    RT_LT,// <
    RT_GE,// >=
    RT_LE,// <=

    RT_RANGE, //:

    RT_ASSIGN,// <-
    RT_IDX, //[] (изначально обрабатываются квадратные скобки, потом получается вот это оператор
    RT_IDX_ASSIGN, //Для операций типа x[1] <- 2

    RT_BR_OP,//(
    RT_BR_CL,//)

    RT_IDX_OP,//[
    RT_IDX_CL,//]

    RT_BLOCK_OP,//{
    RT_BLOCK_CL,//}

    RT_REPEAT,//repeat
    RT_BREAK,//break
    RT_IF,//if
    RT_ELSE,//else

    RT_COMMA//,
};

enum rtoken_basetype
{
    RTB_ID,
    RTB_CONST,
    RTB_OP,
    RTB_FUNC,
    RTB_BRACKET,
    RTB_RWORD,
    RTB_COMMA,
};

enum rop_priority //приоритет операций
{
    ROP_ANDOR = 2,
    ROP_NOT = 5,
    ROP_PLUSMINUS = 2,
    ROP_MULDIV = 3,
    ROP_CMP = 1,
    ROP_RANGE = 4,
    ROP_ASSIGN = 0
};

class RToken
{
private:
    rtoken_t type;
    std::string data;
    rop_priority priority;
    int minArgs; //минимальное кол-во аргументов
    int maxArgs;//макс. кол-во аргументов
    int argsExpected; //сюда засовывается число аргументов в функции
    bool isLeftAssociated; //левоасс. или правоассоц. оператор
    rtoken_basetype basetype;

    RVec vector; //когда распарсим вектор, сохраним то, что распарсили

public:
    friend class RFuncOp;
    friend class RInt;//интерпретатор
    friend class RParser;//парсер

    RToken(){}
    RToken(const RToken&);
    RToken(const std::string& str, const bool plainString=false);
    RToken(const RVec&);
    ~RToken(){}

    const RVec getVector() const throw(UndefVarException); //получить вектор

    RVar *getRVar() const throw(RTypeException); //если нужно из токена типа число или строка сделать Rvar
};

#endif // RTOKEN_H
