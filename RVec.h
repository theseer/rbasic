#ifndef RVEC_H
#define RVEC_H

#include "RVar.h"

#include <string>
#include <vector>
#include <algorithm>

class RVec //реализация контейнера, все хранится в них
{
private:
    std::vector<RVar*> data;
	rtype type;	

    void init(const std::vector<RVar*>&, rtype);
    RVar *copyVar(const RVar *var) const throw(RTypeException);
    RVec(const std::vector<RVar*>& _data, const rtype _type) : data(_data), type(_type){} //В приватных, потому что не совсем безопасен (при вызове деструктора удалить все объекты по указателям), а нужен только в методах класса

    void destroyData();

public:
	RVec();
    RVec(const RVec&);
	RVec(const std::vector<double>&);
	RVec(const std::vector<std::string>&);
	RVec(const std::vector<bool>&);
    RVec(const rtype _type, int _size=0); //заданного типа, содержищий _size NULL-ов
	RVec(const std::vector<RNum>&);
	RVec(const std::vector<RChar>&);
    RVec(const std::vector<RLog>&);
    RVec(const RNum&);
    RVec(const RLog&);
    RVec(const RChar&);
	RVec(const double); //создать RVec из одного дабла
	RVec(const std::string&);
	RVec(const bool);
    RVec(const double, const double); //оператор двоеточие, например, Rvec (1, 5) должен создать вектор 1, 2, 3, 4 (1:5)
	~RVec();	
	
	RVec& operator=(const RVec&);
	
	const RVec length() const;	
	const RVec mode() const;//вернет вектор, в котором написан тип вектора, например, для чисел "numeric"
	
	std::string toString() const; //весь вектор привести к строке, нужно для печати
	
	const RVec operator-() const throw(RTypeException);
	const RVec operator~() const throw(RTypeException);
	
	const RVec operator&(const RVec&) const throw(RTypeException);
	const RVec operator|(const RVec&) const throw(RTypeException);
	
	const RVec operator+(const RVec&) const throw(RTypeException);
	const RVec operator-(const RVec&) const throw(RTypeException);
	const RVec operator*(const RVec&) const throw(RTypeException);
    const RVec operator/(const RVec&) const throw(RTypeException);
	
	const RVec operator==(const RVec&) const;
	const RVec operator!=(const RVec&) const;
	const RVec operator>(const RVec&) const;
	const RVec operator>=(const RVec&) const;
	const RVec operator<(const RVec&) const;
	const RVec operator<=(const RVec&) const;
	
	RVec& operator+=(const RVec&) throw(RTypeException);
	RVec& operator-=(const RVec&) throw(RTypeException);
	RVec& operator*=(const RVec&) throw(RTypeException);
	RVec& operator/=(const RVec&) throw(RTypeException);
	RVec& operator&=(const RVec&) throw(RTypeException);
	RVec& operator|=(const RVec&) throw(RTypeException);
	
    RVec operator[](const RVec&) const throw(RTypeException); //вектор 1, 2, 3, 4, 5... в квадратных скобках передали 0, 0, 2 => вернем вектор (1, 1, 3); если логический тип, то, если видим 0, то не берем, если 1, то берем
	
	RVec& setValue(const int, const RVec&) throw(RTypeException); //Rvec по заданию
    RVar *getValue(const int) const  throw(NullTypeException);

    void append(const RVar *elem) throw(RTypeException); //первый элемент вектора elem положить в конец
	
	RVec& cvtType(const rtype) throw(RTypeException); //преобразовать тип вектора к переданному rtype
};

#endif 
