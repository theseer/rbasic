#ifndef RBASIC_H
#define RBASIC_H

#include <exception>
#include <string>

class UndefVarException : std::exception //класс для исключений необъявленной переменной, которые бросаются при обращении к переменной, которой нет
{
public:
    std::string message;
    UndefVarException(std::string _message) : message(_message){}
    ~UndefVarException() throw(){}
};

enum rerr_codes
{
    RERR_UNDEF_VAR = 1, //Попытка доступа к необъявленной переменной
    RERR_BAD_TYPE = 2, //Плохой тип
    RERR_UNEXP_TOKEN = 3, //Синтакс. ошибка, например, не там запятая
    RERR_UNEXP_ID = 4, //В неправильном месте идентификатор
    RERR_IO = 5, //Ввод-вывод
    RERR_BAD_ARGS = 6, //Неверное число аргументов
    RERR_NOT_IMPLEMENTED = 7 //Не реализованные части языка (блоки операторов, if, repeat...)
};

#endif // RBASIC_H
