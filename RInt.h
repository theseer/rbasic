#ifndef RINT_H
#define RINT_H

#include <map>
#include <stack>
#include <queue>
#include <vector>
#include <string>
#include <ostream>
#include <cassert>
#include <iostream>

#include "RBasic.h"
#include "RToken.h"

class RVec;

class RFuncOp
{
public:
    //args - вектор токенов
    RToken c(const std::vector<RToken>& args); //создает вектор из аргументов, c(1,2,3) => vec =[1,2,3]
    RToken length(const std::vector<RToken>& args);
    RToken mode(const std::vector<RToken>& args);

    RToken plus(const std::vector<RToken>& args);
    RToken minus(const std::vector<RToken>& args);
    RToken mul(const std::vector<RToken>& args);
    RToken div(const std::vector<RToken>& args);

    RToken lnot(const std::vector<RToken>& args);
    RToken land(const std::vector<RToken>& args);
    RToken lor(const std::vector<RToken>& args);

    RToken eq(const std::vector<RToken>& args);
    RToken ne(const std::vector<RToken>& args);
    RToken gt(const std::vector<RToken>& args);
    RToken lt(const std::vector<RToken>& args);
    RToken ge(const std::vector<RToken>& args);
    RToken le(const std::vector<RToken>& args);

    RToken range(const std::vector<RToken>& args);

    RToken assign(const std::vector<RToken>& args); //присваивание
    RToken idx(const std::vector<RToken>& args); //взятие индекса
    RToken idxAssign(const std::vector<RToken> &args); //Присваивание по индексу
};

class RInt
{
private:

    typedef RToken(RFuncOp::*RFunc)(const std::vector<RToken>&);

    RInt();
    RInt(const RInt&);
    void operator=(const RInt&);

    RFuncOp forCall;

    std::stack<RToken> stack;
    std::queue<RToken> queue;

    std::map<std::string, RVec> vars;
    std::map<std::string, RFunc> functions;
    std::map<std::string, RFunc> operators;
    std::vector<std::string> funcNames;

    void initFunctions(); //инициализация мапов
    void initOps();

	//в мапе ключом будет строка, а возвращает функцию

public:

    friend class RFuncOp;
    friend class RToken;

    static RInt& getInstance()
    {
        static RInt instance;
        return instance;
    }

    ~RInt(){}

    std::string runQueue();
    std::string runQueue(const std::queue<RToken>& runqueue);
    void setQueue(const std::queue<RToken>& value);
    std::queue<RToken>& getQueue();

    const RVec getVar(const std::string& name) throw(UndefVarException); //получили имя, ищем в мапе vars
    void setVar(const std::string& name, const RVec& value); //то же самое, только устанавливает в vars
};

#endif
